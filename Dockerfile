# 镜像文件
FROM golang:latest
# 维修者
MAINTAINER nelsons "nelsons.goole@gmail.com"

# 镜像中项目路径
WORKDIR $GOPATH/src/chinaase.com/helloworld
# 拷贝当前目录代码到镜像
COPY . $GOPATH/src/chinaase.com/helloworld
# 制作镜像
RUN go build .

# 暴露端口
EXPOSE 8001

# 程序入口
ENTRYPOINT ["./helloworld"]