package main

import "net/http"

func main() {

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("hello gitlab cicd, my name is nelsons  "))
	})

	http.ListenAndServe(":8001", nil)
}